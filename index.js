'use strict';
require('dotenv').config();
const snoowrap = require('snoowrap');
const _ = require('lodash');

const r = new snoowrap({
  user_agent: process.env.USER_AGENT,
  client_id: process.env.CLIENT_ID,
  client_secret: process.env.CLIENT_SECRET,
  refresh_token: process.env.REFRESH_TOKEN
});

// Might have to just xpost to these, not sure if other subreddits would allow xposts from a bot
const botFriendlySubreddits = ['automate', 'subredditsimulator', 'DisciplesOfTotes', 'AwesomeBots', 'AInotHuman', 'scp'];

// Might expand this later
const possibleCommands = ['post-to'];

const postToExpression = new RegExp('(' + possibleCommands.join("") + '):?(\/r\/[^\s"]+)+', 'gi');
const subredditExpression = /(\/r\/[^\s]+)+/ig;
const testSubreddit = 'auto_xposter_test';

const replyToComand = function(){};
const submitToSubreddit = function(data){
  r.get_subreddit(data.subreddit).submit_link({
    title: data.title,
    url: data.url
  }).then(console.log);
};

const checkBodyContent = function(val) {
  let matches = val.match(postToExpression);
  if(matches.length) {
    matches = _.uniq(matches);
    matches = _.map(matches, function(match) {
      let subreddit = match.trim().match(subredditExpression);
      if(subreddit.length) {
        subreddit = subreddit[0].replace("/r/", "");
        let data = {subreddit: testSubreddit, title: '<Original Title> x-post: <Original Subreddit>', url: 'http://original-url?from=xpost-bot'};
        submitToSubreddit(data);
        replyToComand();
      }
    });
  }
};

// Test post
// r.get_submission('4zjtk0').selftext.then(selftext => {
//   checkBodyContent(selftext);
// });

r.get_new_comments().then(comment_listing => {
  comment_listing.forEach(function (comment) {
    console.log(comment.body);
    //checkBodyContent(comment);
  });
});
